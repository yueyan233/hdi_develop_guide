# IDL继承功能开发指导
IDL当前支持继承功能，在新版本中的仅描述新增接口，不用将旧版本的接口复制过来。
## 继承功能使用方法
### idl
在idl中使用继承时使用的关键字为extends
- 在import中导入被继承的接口
- 使用extends关键字去继承接口
#### v1_0
```
package ohos.hdi.foo.v1_0;

import ohos.hdi.foo.v1_0.Types;
import ohos.hdi.foo.v1_0.IFooCallback;

interface IFoo {
    Ping([in] String sendMsg,[out] String recvMsg);

    GetData([out] struct FooInfo info);

    SetCallback([in] IFooCallback cbObj);
}
```
#### v1_1
```
package ohos.hdi.foo.v1_1;

import ohos.hdi.foo.v1_0.IFoo;
import ohos.hdi.foo.v1_1.Types;
import ohos.hdi.foo.v1_1.IFooCallback;

interface IFoo extends ohos.hdi.foo.v1_0.IFoo {
    PingAppend([in] String sendMsg,[out] String recvMsg);

    GetDataAppend([out] struct FooInfoAppend info);

    SetCallbackAppend([in] IFooCallback cbObj);
}
```
### BUILD.gn
#### C++
C++侧使用继承功能，请添加imports，该配置用于生成继承与被继承版本所编译出的so的依赖关系

//drivers/interface/foo/BUILD.gn
```
import("//drivers/hdf_core/adapter/uhdf2/hdi.gni")
if (defined(ohos_lite)) {
  group("libfoo_proxy_1.1") {
    deps = []
    public_configs = []
  }
} else {
  hdi("foo") {
    module_name = "foo_service"
    imports = [ "ohos.hdi.foo.v1_0:foo" ] # 生成两个版本的so依赖关系

    sources = [
      "IFoo.idl",
      "IFooCallback.idl",
      "Types.idl",
    ]

    language = "cpp"
    subsystem_name = "hdf"
    part_name = "drivers_interface_foo"
  }
}
```
C++侧使用继承时编译得到的so有依赖关系，如下图所示：
![](pictures/cpp_extends_picture.png)
#### C
C侧使用继承功能，是在工具层面对两个idl文件进行整合，其编译得到的so无依赖关系，因此不需要添加imports。  
C侧使用继承示意图：
![](pictures/c_extends_picture.png)



