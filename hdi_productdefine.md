# HDI产品化配置 #



### HDI产品化配置生成视图

![](pictures/hdi_productdefine.svg)



### 产品化配置

//productdefine/common/inherit/rich.json

```json
{
  "component": "drivers_interface_foo",
  "features": [ "drivers_interface_foo_feature_passthrough = false" ] 
},
```

drivers_interface_foo_feature_passthrough控制drivers_interface_foo部件是否生成stub库，值为false表示生成，true不生成

//productdefine/common/inherit/chipset_common.json

```json
{
  "component": "drivers_peripheral_foo",
  "features": ["drivers_peripheral_foo_feature_passthrough = false"]
},
```

drivers_peripheral_foo_feature_passthrough控制drivers_peripheral_foo部件是否生成stub库，值为false表示生成，true不生成



### 系统部件修改

//drivers/interface/foo/bundle.json

```json
{
    "name": "drivers_interface_foo",
    "description": "foo device driver interface",
    "version": "3.2",
    "license": "Apache License 2.0",
    "component": {
      "name": "drivers_interface_foo",
      "subsystem": "hdf",
      "features" : [ "drivers_interface_foo_feature_passthrough" ], # 添加feature
      "syscap": [""],
      "adapter_system_type": ["standard"],
      "rom": "675KB",
      "ram": "1024KB",
      "deps": {
        "components": [
          "ipc",
          "hdf_core",
          "hiviewdfx_hilog_native",
          "utils_base"
        ],
        "third_part": [
          "bounds_checking_function"
        ]
      },
      "build": {
        "sub_component": [
          "//drivers/interface/foo/v1_0:foo_idl_target"
        ],
        "test": [
        ],
        "inner_kits": [
          {
            "name": "//drivers/interface/foo/v1_0:libfoo_proxy_1.0",
            "header": {
              "header_files": [
              ],
              "header_base": "//drivers/interface/foo"
            }
          },
          {
            "name": "//drivers/interface/foo/v1_0:foo_idl_headers",
            "header": {
              "header_files": [
              ],
              "header_base": "//drivers/interface/foo"
            }
          }
        ]
      }
    }
  }
```



//drivers/interface/foo/v1_0/BUILD.gn

```
# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//drivers/hdf_core/adapter/uhdf2/hdi.gni")

# 1、声明feature
declare_args() {
  drivers_interface_foo_feature_passthrough = false
}

if (defined(ohos_lite)) {
  group("libfoo_proxy_1.0") {
    deps = []
    public_configs = []
  }
} else {
  hdi("foo") {
    module_name = "foo_service"

    sources = [
      "IFoo.idl",
      "Types.idl",
      "IFooCallback.idl",
    ]

	# 2、根据feature设置编译模式
    if (drivers_interface_foo_feature_passthrough) {
      mode = "passthrough"
    }

    language = "cpp"
    subsystem_name = "hdf"
    part_name = "drivers_interface_foo"
  }
}
```



### 芯片部件修改

//drivers/peripheral/foo/bundle.json

```json
{
    "name": "drivers_peripheral_foo",
    "description": "foo device driver",
    "version": "3.1",
    "license": "Apache License 2.0",
    "component": {
      "name": "drivers_peripheral_foo",
      "subsystem": "hdf",
      "features" : [ "drivers_peripheral_foo_feature_passthrough" ], # feature配置
      "syscap": [""],
      "adapter_system_type": ["standard"],
      "rom": "675KB",
      "ram": "7400KB",
      "deps": {
        "components": [
          "ipc",
          "device_driver_framework",
          "hiviewdfx_hilog_native",
          "utils_base"
        ],
        "third_part": [
          "bounds_checking_function"
        ]
      },
      "build": {
        "sub_component": [
          "//drivers/peripheral/foo:foo_entry"
        ],
        "test": [
          "//drivers/peripheral/foo/test:foo_hdi_test"
        ],
        "inner_kits": [
        ]
      }
    }
  }
```





//drivers/peripheral/foo/hdi_service/BUILD.gn

```
# Copyright (C) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

# 1、feature声明
declare_args() {
  drivers_peripheral_foo_feature_passthrough = false
}

ohos_shared_library("libfoo_service_1.0") {
  sources = [ "foo_impl.cpp" ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_interface_foo:foo_idl_headers",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "ipc:ipc_single",
    ]
  } else {
    external_deps = [
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_foo"
}

# 2、非passthrough模式下生成libfoo_driver.z.so
if (!drivers_peripheral_foo_feature_passthrough) {
  ohos_shared_library("libfoo_driver") {
    sources = [ "foo_driver.cpp" ]

    deps = [ "//drivers/interface/foo/v1_0:libfoo_stub_1.0" ]

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "hdf_core:libhdf_host",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdi",
        "hiviewdfx_hilog_native:libhilog",
        "ipc:ipc_single",
      ]
    } else {
      external_deps = [
        "hilog:libhilog",
        "ipc:ipc_single",
      ]
    }

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_foo"
  }
}

group("hdi_foo_service") {
  deps = [ ":libfoo_service_1.0" ]

  # 3、非passthrough模式下libfoo_driver.z.so参与编译
  if (!drivers_peripheral_foo_feature_passthrough) {
    deps += [ ":libfoo_driver" ]
  }
}
```