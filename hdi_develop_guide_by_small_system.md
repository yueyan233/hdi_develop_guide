# small(L1)系统HDI开发指导



## 简介

在small系统上，HDI仅支持直通模式形态，并支持C/C++，本文讲述如何在L1系统上进行HDI的开发。



## HDI开发步骤

本文档以C++侧用户态驱动IPC模式开发实例展示HDI开发流程。



### 定义HDI接口

idl语法说明请参考：[LINK](https://developer.harmonyos.com/cn/docs/documentation/doc-references/idl-file-structure-0000001050722806)



idl接口定义：

在drivers_interface仓下添加对应模块的idl文件。

```
master$ tree ./drivers/interface/foo
./drivers/interface/foo
├── bundle.json
└── v1_0
    ├── BUILD.gn         # 编译idl文件的BUILD.gn
    ├── IFoo.idl         # 定义驱动接口
    ├── IFooCallback.idl # 定义用于回调的接口
    └── Types.idl        # 定义自定义类型数据
```



IFoo.idl

```
package ohos.hdi.foo.v1_0;

import ohos.hdi.foo.v1_0.Types;
import ohos.hdi.foo.v1_0.IFooCallback;

interface IFoo {
    Ping([in] String sendMsg,[out] String recvMsg);

    GetData([out] struct FooInfo info);

    SendCallback([in] IFooCallback cb);

    Echo();
};
```



IFooCallback.idl

```
package ohos.hdi.foo.v1_0;

import ohos.hdi.foo.v1_0.Types;

[callback] interface IFooCallback {
    Notify([in] String msg);
};
```



Types.idl

```
package ohos.hdi.foo.v1_0;

enum FooType {
    TYPE_ONE,
    TYPE_TWO,
    TYPE_THREE,
};

struct FooInfo {
    unsigned int id;
    String name;
    enum FooType type;
};
```



#### idl编译配置

配置文件//drivers/interface/foo/v1_0/BUILD.gn，内容如下：

```
import("//drivers/interface/interface.gni")  # 导入通用模板

interface("foo") {
    sources = [
        "IFoo.idl",
        "IFooCallback.idl",
        "Types.idl",
    ]

    language = "cpp"
    subsystem_name = "hdf"
    part_name = "drivers_interface_foo"
}
```



#### 创建HDI接口部件

这里给出bundle.json模板进行参考，添加部件配置：//drivers/interface/foo/bundle.json

```
{
    "name": "drivers_interface_foo",
    "description": "foo device driver interface",
    "version": "4.0",
    "license": "Apache License 2.0",
    "component": {
        "name": "drivers_interface_foo",
        "subsystem": "hdf",
        "syscap": [""],
        "adapter_system_type": ["small"],  # 注意系统类型填写small
        "rom": "675KB",
        "ram": "1024KB",
        "deps": {
            "components": [
                "c_utils",
                "hdf_core",
                "hilog_featured_lite",
                "bounds_checking_function"
            ],
            "third_part": []
        },
        "build": {
            "sub_component": [
                "//drivers/interface/foo/v1_0:foo_idl_target"
            ],
            "test": [
            ],
            "inner_kits": [
                {
                  "name": "//drivers/interface/foo/v1_0:foo_idl_headers",
                  "header": {
                    "header_files": [
                    ],
                    "header_base": "//drivers/interface/foo"
                  }
                },
                {
                    "name": "//drivers/interface/foo/v1_0:libfoo_proxy_1.0",
                    "header": {
                    "header_files": [
                    ],
                    "header_base": "//drivers/interface/foo"
                    }
                }
            ]
        }
    }
}
```

其中：

- foo_idl_target包含libfoo_proxy_1.0.z.so与libfoo_stub_1.0.z.so
- libfoo_proxy_1.0为对外接口so
- foo_idl_headers为头文件目标，若只需引用头文件，则进行外部依赖添加



部件编译入口配置

以master分支，ipcamera_hispark_taurus产品为例，配置如下：

//vendor/hisilicon/hispark_taurus/config.json，在hdf子系统下添加drivers_interface_foo部件

```
{
    "subsystem": "hdf",
    "components": [
        { "component": "drivers_interface_foo", "features":[] }
     ]
}
```



#### idl编译及生成产物

以上配置完成后，即可指定部件名进行编译

```
./build.sh --product-name ipcamera_hispark_taurus --build-target drivers_interface_foo
```



##### 编译产物

编译成功后，可在//out/hispark_taurus/ipcamera_hispark_taurus/gen/drivers/interface/foo/v1_0目录下生成HDI源码:

```
$ tree ./out/hispark_taurus/ipcamera_hispark_taurus/gen/drivers/interface/foo/v1_0/
./out/hispark_taurus/ipcamera_hispark_taurus/gen/drivers/interface/foo/v1_0/
├── drivers_interface_foo__libfoo_proxy_1.0_external_deps_temp.json
├── foo_callback_service.cpp  #callback回调接口实现源文件
├── foo_callback_service.h    #callback回调接口实现头文件
├── foo_proxy.cpp             #对外接口源文件
├── foo_service.cpp           #接口实现源文件
├── foo_service.h             #接口实现头文件
├── ifoo.h                    #接口头文件
├── ifoo_callback.h           #callback回调接口头文件
├── libfoo_proxy_1.0__check.d
├── libfoo_proxy_1.0__notice.d
└── types.h                   #自定义类型
```



### HDI服务实现

OH提供的HDI接口，其实现应当在`//drivers/peripheral/`下的对应模块中。

#### 模块添加

按需新增驱动模块目录，以下为参考：

//drivers/peripheral/foo

```
$ tree ./drivers/peripheral/foo/
./drivers/peripheral/foo/
├── BUILD.gn
├── bundle.json  #部件配置
├── hdi_service  # 服务实现目录
│   ├── BUILD.gn
│   ├── foo_impl.cpp  # 接口实现代码
│   └── foo_impl.h
└── test
    ├── BUILD.gn
    └── foo_small_test.cpp #测试用例
```

`foo_impl.h/.cpp`为接口实现头/源文件，参考idl生成的代码（foo_service.h/.cpp）按需修改。



接口实现

//drivers/peripheral/foo/hdi_service/foo/foo_impl.h

```c++
#ifndef OHOS_HDI_FOO_V1_0_FOO_IMPL_H
#define OHOS_HDI_FOO_V1_0_FOO_IMPL_H

#include "v1_0/ifoo.h"

namespace OHOS {
namespace HDI {
namespace Foo {
namespace V1_0 {
class FooImpl : public IFoo {  // 继承接口抽象类
public:
    FooImpl() = default;
    virtual ~FooImpl() = default;

    int32_t Ping(const std::string& sendMsg, std::string& recvMsg) override;

    int32_t GetData(FooInfo& info) override;

    int32_t SendCallback(const std::shared_ptr<IFooCallback>& cb) override;

    int32_t Echo() override;
};
} // V1_0
} // Foo
} // HDI
} // OHOS

#endif // OHOS_HDI_FOO_V1_0_FOO_IMPL_H
```



//drivers/peripheral/foo/hdi_service/foo/foo_impl.cpp

```c++
#include "foo_impl.h"
#include <hdf_base.h>
#include <hdf_log.h>

#define HDF_LOG_TAG    foo_service

namespace OHOS {
namespace HDI {
namespace Foo {
namespace V1_0 {
extern "C" IFoo *FooImplGetInstance(void)
{
    return new (std::nothrow) FooImpl();
}

int32_t FooImpl::Ping(const std::string& sendMsg, std::string& recvMsg)
{
    recvMsg = sendMsg;
    return HDF_SUCCESS;
}

int32_t FooImpl::GetData(FooInfo& info)
{
    info.id = 1;
    info.name = "foo";
    info.type = V1_0::FooType::TYPE_THREE;
    return HDF_SUCCESS;
}

int32_t FooImpl::SendCallback(const std::shared_ptr<IFooCallback>& cb)
{
    if (cb == nullptr) {
        HDF_LOGE("[%s:%d] cb is nullptr", __func__, __LINE__);
        return HDF_FAILURE;
    }

    return cb->Notify("foo callback notify");
}

int32_t FooImpl::Echo()
{
     HDF_LOGI("[%s:%d] echo call success", __func__, __LINE__);
    return HDF_SUCCESS;
}
} // V1_0
} // Foo
} // HDI
} // OHOS
```

以上为示例，开发者按需填充业务代码。



#### 编译配置

//drivers/peripheral/foo/hdi_service/BUILD.gn

```
# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")

ohos_shared_library("libfoo_service_1.0") {
  sources = [ "foo_impl.cpp" ]

  external_deps = [
    "hdf_core:libhdf_utils",
    "hdf_core:libhdi",
    "hilog_featured_lite:hilog_shared",
    "drivers_interface_foo:foo_idl_headers",
    "bounds_checking_function:libsec_shared",
  ]

  subsystem_name = "hdf"
  part_name = "drivers_peripheral_foo"
}

group("foo_service") {
  deps = [ ":libfoo_service_1.0" ]
}
```



//drivers/peripheral/foo/BUILD.gn

```
# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

group("foo_entry") {
  deps = [ "//drivers/peripheral/foo/hdi_service:foo_service" ]
}
```



#### 部件配置

新建`drivers/peripheral/foo/build.json`用于定义新增的drivers_peripheral_foo部件：

```
{
    "name": "drivers_peripheral_foo",
    "description": "foo device driver",
    "version": "4.0",
    "license": "Apache License 2.0",
    "component": {
      "name": "drivers_peripheral_foo",
      "subsystem": "hdf",
      "syscap": [""],
      "adapted_system_type": ["small"], #注意系统类型填写small
      "rom": "675KB",
      "ram": "7400KB",
      "deps": {
        "components": [
          "c_utils",
          "hdf_core",
          "hilog_featured_lite",
          "bounds_checking_function",
          "drivers_interface_foo"
        ],
        "third_part": []
      },
      "build": {
        "sub_component": [
          "//drivers/peripheral/foo:foo_entry"
        ],
        "test": [
          "//drivers/peripheral/foo/test:foo_hdi_test"
        ],
        "inner_kits": [
        ]
      }
    }
  }
```

注意部件依赖，在components中填入依赖的部件信息



### 部件编译入口配置

以master分支，ipcamera_hispark_taurus产品为例，配置如下：

//vendor/hisilicon/hispark_taurus/config.json，在hdf子系统下添加drivers_peripheral_foo部件:

```
{ "component": "drivers_peripheral_foo", "features":[] }
```



#### 服务代码编译

```
./build.sh --product-name ipcamera_hispark_taurus --build-target drivers_peripheral_foo
```



### HDI接口调用

本章节主要介绍HDI接口调用方式。

#### 服务获取

c++侧接口

```
class IFoo : public HdiBase {
public:
    DECLARE_HDI_DESCRIPTOR(u"ohos.hdi.foo.v1_0.IFoo");

    virtual ~IFoo() = default;

	// 调用IFoo::Get(true)获取HDI对象
    static std::shared_ptr<OHOS::HDI::Foo::V1_0::IFoo> Get(bool isStub = false);
    // 可指定名称，调用IFoo::Get("foo_service", true)获取HDI对象
    static std::shared_ptr<OHOS::HDI::Foo::V1_0::IFoo> Get(const std::string &serviceName, bool isStub = false);

    virtual int32_t Ping(const std::string& sendMsg, std::string& recvMsg) = 0;

    virtual int32_t GetData(FooInfo& info) = 0;

    virtual int32_t SendCallback(const std::shared_ptr<OHOS::HDI::Foo::V1_0::IFooCallback>& cb) = 0;

    virtual int32_t GetVersion(uint32_t& majorVer, uint32_t& minorVer)
    {
        majorVer = 1;
        minorVer = 0;
        return HDF_SUCCESS;
    }

    virtual const std::u16string GetDesc()
    {
        return metaDescriptor_;
    }
};
```



c侧接口

```
struct IFoo {
    ...
};

// 获取默认服务名为"foo_service"的HDI服务，需调用IFooGet(true)获取HDI对象
struct IFoo *IFooGet(bool isStub);
// 功能同上，但可指定服务名
struct IFooGetInstance(const char *serviceName, bool isStub);

// 释放HDI服务对象
void IFooRelease(struct IFoo *instance, bool isStub);
// 释放指定服务名对应的服务对象
void IFooReleaseInstance(const char *serviceName, struct IFoo *instance, bool isStub);
```



#### 接口调用

c++侧调用示例

```
#include "v1_0/ifoo.h"

std::shared_ptr<V1_0::IFoo> foo_service = V1_0::IFoo::Get(true);  //获取HDI对象

std::string recvMsg;
int32_t ret = foo_service->Ping("hello", recvMsg); // HDI接口调用
```



C测调用示例：

```
#include "v1_0/ifoo.h"

struct IFoo *foo_service = IFooGet(true);

const uint32_t recvLen = 10;
char recvMsg[recvLen] = {0};
int32_t ret = foo_service->Ping(foo_service, "hello", recvMsg, recvLen);
```





