# HDI直通模式开发指导

HDI当前支持C/C++的IPC/Passthrough模式，IPC模式兼容Passthrough模式，亦可指定生成Passthrough模式，该模式不兼容IPC模式。



## HDI开发步骤

本文档已C++侧直通模式开发为例展示HDI开发流程：

### HDI系统部件开发

此部件开发与ipc模式下基本相同，我们已[hdi_develop_guide.md](hdi_develop_guide)中系统部件开发为示例，不同点在于，用于编译IDL文件的BUILD.gn需要指定编译模式:

//drivers/interface/foo/BUILD.gn

```
import("//drivers/hdf_core/adapter/uhdf2/hdi.gni")
hdi("foo") {
    module_name = "foo"
    sources = [
        "IFoo.idl",
        "IBar.idl",
        "IFooCallback.idl",
        "MyTypes.idl",
    ]

    language = "cpp"
    mode = "passthrought"                       # 设置编译模式为passthrough
    subsystem_name = "hdf"
    part_name = "drivers_interface_foo"
}
```



passthrought模式下HDI源码生成图示：

![](pictures/passthrough_gen_c++_code.svg)

注意：

- 绿色标注的头文件为对外头文件，其他头文件不对外
- libfoo_proxy_1.0.z.so为系统接口so
- xxx_service.h/cpp为接口实现代码，为模板代码仅供参考，不参与编译



### HDI芯片部件开发

此部件开发与ipc模式下基本相同，我们已[hdi_develop_guide.md](hdi_develop_guide)中系统部件开发为示例，不同点在于，无需编译驱动加载的so库，只需编译接口实现的so库：

#### 模块添加

//drivers/peripheral/foo

```
./peripheral/foo
├── BUILD.gn       # 模块编译BUILD.gn
├── bundle.json    # 部件化配置
├── hdi_service    # hdi服务代码
│   ├── BUILD.gn        # hdi服务代码编译BUILD.gn
│   ├── bar_impl.cpp    # IBar接口实现源文件
│   ├── bar_impl.h      # IBar接口实现头文件
│   ├── foo_impl.cpp    # IFoo接口实现源文件
│   └── foo_impl.h      # IFoo接口实现头文件
└── test           # TDD测试用例
    ├── BUILD.gn               # 测试用例代码编译BUILD.gn
    ├── foo_callback_impl.cpp  # callback实现源文件
    ├── foo_callback_impl.h    # callback实现头文件
    └── foo_hdi_test.cpp       # TDD测试用例代码
```



#### 编译配置

//drivers/peripheral/foo/hdi_service/BUILD.gn

```
# Copyright (C) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

ohos_shared_library("libfoo_service_1.0") {

  sources = [
    "foo_impl.cpp",
    "bar_impl.cpp",
  ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_interface_foo:foo_idl_headers",
      "hiviewdfx_hilog_native:libhilog",
    ]
  } else {
    external_deps = [
      "hilog:libhilog",
    ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_foo"
}

group("hdi_foo_service") {
  deps = [
    ":libfoo_service_1.0",
  ]
}
```



#### HDI基础图示

HDI基础的so包括以下：

![](pictures/passthrough_so_deps.svg)

### HDI接口调用

与IPC模式基本相同，不同点在于Get获取服务对象是传参需指定为true:

c++侧调用示例

```
#include <string>
#include "v1_0/ifoo.h" // 包含HDI接口头文件
using namespace OHOS::HDI::Foo::V1_0; // HDI命名空间

sptr<IFoo> fooService = IFoo::Get(true);  // 获取HDI对象，以passthrough模式

int32_t ret = fooService->Ping("hello");  // 调用接口
```



c侧调用示例：

```
#include "ifoo.h"

struct IFoo *fooService = IFooGet(true); // passthrough模式

int32_t ret = fooService->Ping("hello world");

IFooRelease(fooService, true);
```



## 其他

- 无需添加驱动配置（pass、group、selinux、hcs等）
- 无需编译driver入口so
- 无法使用死亡监听、服务状态监听、HDI服务动态加载等功能



## 常见问题



#### service库动态加载失败

service库指接口实现库，Get(true)指定直通模式后，以dlopen的方式获取service对象，而dlopen的库名采用接口描述和服务名转换而来，即需按一定规则对service库进行命名，当命名不规范是，会出现以下错误日志：

```
08-22 11:22:51.046  1816  1816 E C02500/load_hdi: ParseInterface invalid hdi impl so name /vendor/lib/libfoo_service_1.0.z.so  // 这里的库名为转换拼接而成，需要和提供的service库名相等
```

service库命名规则参考[hdi_develop_guide.md](hdi_develop_guide)中的接口实现库名规则章节
