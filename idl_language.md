# IDL



## IDL简介

当客户端与服务器通信时，需要定义双方都认可的接口，以保障双方可以成功通信。HarmonyOs IDL(Interface Definition Language) 则是一种定义此类接口的语言。



HarmonyOS IDL先把需要传递的对象分解成操作系统能够理解的基本类型，并根据开发者的需要封装跨边界的对象。在HarmonyOS中，HarmonyOS IDL接口包含面向应用程序的北向接口和面向硬件设备的南向接口。



HarmonyOs IDL接口描述语言主要用于：

- 声明系统服务对外提供的服务接口，根据接口声明在编译时生成跨进程调用（IPC）或跨设备调用（RPC）的代理（Proxy）和桩（Stub）的C/C++代码。

- 声明Ability对外提供的服务接口，根据接口声明在编译时生成跨进程调用（IPC）或跨设备调用（RPC）的代理（Proxy）和桩（Stub）的C/C++代码或Java代码。



使用HarmonyOS IDL接口描述语言声明接口具有以下优点：

- HarmonyOS IDL中是以接口的形式定义服务，可以专注于定义而隐藏实现细节。
- HarmonyOS IDL中定义的接口可以支持跨进程调用或跨设备调用。根据HarmonyOS IDL中的定义生成的信息或代码可以简化跨进程或跨设备调用接口的实现。



## IDL构成

IDL 描述的接口代码示例如下：

IFoo.idl

```
package ohos.hdi.foo.v1_0;  // 包名

import ohos.hdi.foo.v1_0.IFooCallback;  //导入IFooCallback.idl
import ohos.hdi.foo.v1_0.MyTypes;       //导入MyTypes.idl

sequenceable ohos.hdi.foo.v1_0.IFooAbilityInfo; // 导入sequenceable类型

interface IFoo {
    Ping([in] String sendMsg, [out] String recvMsg);  //接口方法，in、out分别表示入参、出参

    GetData([out] struct FooInfo info);

    SendCallback([in] IFooCallback cbObj);
}
```

说明：



-  IDL接口描述文件使用C注释。 
-  IDL接口描述文件是以"*.idl"为扩展名的文件。 
-  IDL接口描述文件目录层级必须严格按照包名的层次进行定义，例如：IFoo.idl中的”package foo.v1_0;“对应foo/v1_0/IFoo.idl.idl 
-  "import foo.v1_0.IFooCallback;"表示IFoo.idl导入foo/v1_0/IFooCallback.idl文件。并可以使用IFooCallback.idl文件中定义的类型。 
-  "sequenceable foo.v1_0.IFooAbilityInfo;"表示导入一种可序列化的类IFooAbilityInfo，开发者需手动提供该类的C++文件。 
-  IDL接口描述文件主要以类名命名，例如：IFoo.idl中接口类名为IFoo，特殊的MyTypes.idl则为自定义类型数据的IDL文件。 
-  接口声明无需返回值。 
-  "[in]"、"[out]"表示参数为输入参数还是输出参数。 



### IDL注释

IDL注释采用C/C++风格注释，支持单行注释和多行注释：

```
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.hdi.foo.v1_0;

interface IFoo {
    // 单行注释
    
    /*
     * 多行注释
     */
    Echo([in] String sendMsg, [out] String recvMsg);
}
```



### IDL数据类型

HarmonyOS IDL支持的数据类型可分为：基本数据类型、自定义类型、声明的Sequenceable数据类型、数组类型、容器类型、callback类型、接口类型、共享内存队列、共享内存。



#### 基本数据类型

IDL基本数据类型与Java数据类型、C/C++数据类型的对应关系如下表所示：

| IDL基本数据类型 | C++数据类型 | C数据类型 | Java数据类型 | 数据长度(bytes) |
| --------------- | ----------- | --------- | ------------ | --------------- |
| boolean         | bool        | bool      | boolean      | 1               |
| byte            | int8_t      | int8_t    | byte         | 1               |
| short           | int16_t     | int16_t   | short        | 2               |
| int             | int32_t     | int32_t   | int          | 4               |
| long            | int64_t     | int64_t   | long         | 8               |
| float           | float       | float     | float        | 4               |
| double          | double      | double    | double       | 8               |
| String          | std::string | cstring   | String       | NA              |
| unsigned char   | uint8_t     | uint8_t   | 不支持       | 1               |
| unsigned short  | uint16_t    | uint16_t  | 不支持       | 2               |
| unsigned int    | uint32_t    | uint32_t  | 不支持       | 4               |
| unsigned long   | uint64_t    | uint64_t  | 不支持       | 8               |



#### 自定义类型

自定义类型是指使用关键字enum、struct、union定义的枚举类型、结构体类型、联合体类型，以及这些类型组合嵌套定义的类型。自定义类型文件MyTypes.idl示例如下：

```
package foo.v1_0;

enum BinaryNum {
    BIN_A = -0b001,
    BIN_B = +0b010,
    BIN_C = 0b0011,
};

enum OctNum {
    OCT_A = -01,
    OCT_B = +02,
    OCT_C = 03,
};

enum DecNum {
    DEC_A = -1,
    DEC_B = +2,
    DEC_C = 3,
};

enum HexNum {
    HEX_A = -0x1,
    HEX_B = 0x2,
    HEX_C = ~0X3,
};

enum ExprNum {
    EXPR_A = 1 + 0,
    EXPR_B = 3 - 1,
    EXPR_C = 3 * 1,
    EXPR_D = 4 / 1,
    EXPR_E = 9 % 5,
    EXPR_F = 10 << 2,
    EXPR_G = 100 >> 2,
    EXPR_H = 0xFF & 0x1,
    EXPR_I = 0xFF ^ 0x3,
    EXPR_J = 0xFF | 0x4,
    EXPR_K = (100 + 10) * 10,
};

enum EData : int {
    RED = 1,
    BLUE = 0x0F << 1,
    GREEN = 0b101,
};

struct SData {
    int m1;
    String m2;
    unsigned int[] m3;
    List<String> m4;
    Map<int, String> m5;
};

union UData {
    unsigned char m1;
    unsigned int m2;
};

struct Info {
    struct SData;
    union UData;
    enum EData;
};
```

在定义枚举类型时，需要指定枚举类型的基本数据类型，可以使用byte、short、int、long、unsigned char、unsigned short、unsigned int、unsigned long。如果未指定，则默认基础类型为int类型。



IDL自定义数据类型与Java数据类型、C/C++数据类型的对应关系如下表所示：

| 自定义数据类型 | Java数据类型 | C++数据类型 | C数据类型 |
| -------------- | ------------ | ----------- | --------- |
| struct         | 不支持       | struct      | struct    |
| union          | 不支持       | union       | union     |
| enum           | 不支持       | enum        | enum      |



#### sequenceable数据类型

sequenceable数据类型是指用“sequenceable”关键字声明的非数据类型，表明该数据可通过Parcel进行夸进程（跨设备）传递。声明形式为“sequenceable namespace.typename;”，namespace是该类型所属的命名空间，内部采用“.”连接不同层级的命名空间，typename是类型名。例如：“sequenceable ohos.hdi.foo.v1_0.ApplicationInfo;”表示ApplicationInfo类型可以通过Parcel进行跨进程传递，其命名空间为OHOS::HDI::Foo::V1_0。

sequenceable数据类型并不在IDL文件中定义，而是定义在C++文件或Java文件中，C语言文件不支持。因此，HDI-GEN工具将根据其声明在生成的C++代码文件中加入“#include "application_info.h"”语句，在生成的Java代码文件中加入“import foo.v1_0.ApplicationInfo;”语句。

IDL sequenceable数据类型与Java数据类型、C/C++数据类型的对应关系如下表所示：

| IDL sequenceable数据类型        | C++数据类型                        | C数据类型 | Java数据类型                     |
| :------------------------------ | ---------------------------------- | --------- | -------------------------------- |
| Sequenceable namespace.typename | class typename : public Parcelable | 不支持    | class typename extend Parcelable |



#### 数组类型

数组类型用“T[]”表示，T可以是基本数据类型、自定义类型、Sequenceable数据类型、接口类型和数组类型。

在C++代码中，数组类型生成为std::vector<T>，在Java代码中，数组类型生成为T[]。

HarmonyOS IDL数组数据类型与Java数据类型、C/C++数据类型的对应关系如下表所示：

| IDL数据类型 | C++数据类型    | C数据类型   | Java数据类型 |
| ----------- | -------------- | ----------- | ------------ |
| T[]         | std::vector<T> | T*,int size | T[]          |



#### 容器类型

当前支持两种容器：List和Map。List容器用法为“List<T>”， Map容器用法为“Map<KT, VT>”，T、KT和VT可以为基本数据类型、自定义类型、Sequenceable类型、接口类型、数组类型或容器类型。

在C++代码中，List容器类型生成为std::vector，Map容器类型生成为std::map。在Java代码中，List容器类型生成为ArrayList，Map容器类型生成为HashMap。

IDL容器数据类型与Java数据类型、C/C++数据类型的对应关系如下表所示：

| HarmonyOS IDL数据类型 | C++数据类型      | C数据类型   | Java数据类型 |
| --------------------- | ---------------- | ----------- | ------------ |
| List<T>               | std::vector<T>   | T*,int size | List<T>      |
| Map<KT, VT>           | std::map<KT, VT> | 不支持      | HashMap      |



#### callback数据类型

当IDL文件中的接口添加[callback]属性时，即可成为可传输的接口类型。示例如下：

```
package ohos.hdi.foo.v1_0;

[callback] interface IFooCallback {
    PushData([in] String message);
}
```



callback接口数据类型可作为参数进行传输，示例如下：

```
package foo.v1_0;

import foo.v1_0.IFooCallback;

interface IFoo {
    SendCallbackObj([in] IFooCallback cbObj);
}
```

callback标识了一个接口生成client，server代码，一端构造callback服务对象，发送至另一端后转换为callback客户端对象，即可通过callback客户端对象主动发送数据，即可实现下层实现中回调到上层服务中。



注意：callback数据类型参数属性必须为in。

#### 接口类型

接口类型与callback类似，不同点在于：

1.接口类型无需声明callback属性

2.服务端构造接口类型对象，发送至客户端，客户端接收后可通过此对象沟通对应的HDI服务，相当于一个匿名服务。

3.接口类型参数必须为out属性。



定义接口类型，示例如下：

```
package ohos.hdi.foo.v1_0;

interface IBar {
    PushData([in] String message);
}
```

传输接口类型参数至客户端：

```
package ohos.hdi.foo.v1_0;

import ohos.hdi.foo.v1_0.IBar;

interface IFoo {
    GetBarService([out] IBar barService);
}
```



#### 共享内存消息队列类型

SharedMemQueue<T>为idl支持的共享内存消息队列类型，当前仅支持C++语言，旨在为系统服务与HDI服务直接数据交换提供高性能的解决方案，IDL共享内存数据类型与Java数据类型，C/C++数据类型的对应关系如下表所示：

| HarmonyOS IDL数据类型 | C++数据类型                        | C数据类型 | Java数据类型 |
| --------------------- | ---------------------------------- | --------- | ------------ |
| SharedMemQueue<T>     | std::shared_ptr<SharedMemQueue<T>> | 不支持    | 不支持       |



共享内存队列头文件路径：

```
drivers/adapter/uhdf2/include/hdi/base/hdi_smq.h
```

##### 使用方法

- SharedMemQueue为单工通信方式，规定一端写入，一端读取
- 支持同步队列（不可溢出，支持阻塞读写）、非同步队列（可溢出，不支持阻塞读写）

```
#include <gtest/gtest.h>
#include <hdf_log.h>
#include <osal_mem.h>
#include <securec.h>
#include "smq_test_proxy.h"

#define HDF_LOG_TAG smq_test

using OHOS::sptr;
using OHOS::HDI::Base::SharedMemQueue;
using OHOS::HDI::Base::SharedMemQueueMeta;
using OHOS::HDI::Base::SmqType;

using namespace testing::ext;
using namespace test::cpp_test::smq_test::v1_0;

static constexpr int SMQ_SIZE = 10;
static constexpr int SMQ_WAIT_TIME = 100;

static sptr<ISmqTest> g_client = nullptr;

class SmqTest : public testing::Test {
public:
    static void SetUpTestCase();
    static void TearDownTestCase() {}
    void SetUp(){}
    void TearDown(){}
};

void SmqTest::SetUpTestCase()
{
    g_client = ISmqTest::Get();
    if (g_client == nullptr) {
        printf("SmqTest: get g_client failed.\n");
    }
}

/*
 * smq type:                             sync
 * smq size:                             10
 * write type:                           Blocking
 * write times:                          5
 * number of elements written each time: 1
 * smq is overflow:                      no
 * wait:                                 no
 */
HWTEST_F(SmqTest, SmqTest_001, TestSize.Level1)
{
    // 创建一个元素类型为Info，容量为SMQ_SIZE的共享内存同步队列
    std::shared_ptr<SharedMemQueue<Info>> smq = std::make_shared<SharedMemQueue<Info>>(SMQ_SIZE, SmqType::SYNCED_SMQ);

    int32_t ec = g_client->SmqTest(smq, 1);
    ASSERT_EQ(ec, HDF_SUCCESS);

    constexpr uint32_t SEND_TIMES = 5;
    for (uint32_t i = 0; i < SEND_TIMES; i++) {
        Info data = {true, i, 3.14};
        HDF_LOGI("%{public}s:write smq message %{public}u", __func__, i);
        int status = smq->Write(&data, 1);
        ASSERT_EQ(status, 0);
    }
}

/*
 * smq type:                             sync
 * smq size:                             10
 * write type:                           Blocking
 * write times:                          20
 * number of elements written each time: 1
 * smq is overflow:                      yes
 * wait:                                 no
 */
HWTEST_F(SmqTest, SmqTest_002, TestSize.Level1)
{
    std::shared_ptr<SharedMemQueue<Info>> smq = std::make_shared<SharedMemQueue<Info>>(SMQ_SIZE, SmqType::SYNCED_SMQ);

    int32_t ec = g_client->SmqTest(smq, 1);
    ASSERT_EQ(ec, HDF_SUCCESS);

    constexpr uint32_t SEND_TIMES = 20;
    for (uint32_t i = 0; i < SEND_TIMES; i++) {
        Info data = {true, i, 3.14};
        HDF_LOGI("%{public}s:write smq message %{public}u", __func__, i);
        int status = smq->Write(&data, 1);
        ASSERT_EQ(status, 0);
    }
}

/*
 * smq type:                             sync
 * smq size:                             10
 * write type:                           Blocking
 * write times:                          20
 * number of elements written each time: 1
 * smq is overflow:                      yes
 * wait:                                 yes
 */
HWTEST_F(SmqTest, SmqTest_003, TestSize.Level1)
{
    std::shared_ptr<SharedMemQueue<Info>> smq = std::make_shared<SharedMemQueue<Info>>(SMQ_SIZE, SmqType::SYNCED_SMQ);

    int32_t ec = g_client->SmqTest(smq, 1);
    ASSERT_EQ(ec, HDF_SUCCESS);

    constexpr uint32_t SEND_TIMES = 20;
    for (uint32_t i = 0; i < SEND_TIMES; i++) {
        Info data = {true, i, 3.14};
        HDF_LOGI("%{public}s:write smq message %{public}u", __func__, i);
        int status = smq->Write(&data, 1, OHOS::MillisecToNanosec(SMQ_WAIT_TIME)); //当队列满时，写入阻塞时间为100毫秒
        ASSERT_EQ(status, 0);
    }
}

/*
 * smq type:                             sync
 * smq size:                             10
 * write type:                           Blocking
 * write times:                          3
 * number of elements written each time: 4
 * smq is overflow:                      yes
 * wait:                                 yes
 */
HWTEST_F(SmqTest, SmqTest_004, TestSize.Level1)
{
    std::shared_ptr<SharedMemQueue<Info>> smq = std::make_shared<SharedMemQueue<Info>>(SMQ_SIZE, SmqType::SYNCED_SMQ);
    constexpr uint32_t ELEMENT_SIZE = 4;

    int32_t ec = g_client->SmqTest(smq, ELEMENT_SIZE);
    ASSERT_EQ(ec, HDF_SUCCESS);

    constexpr uint32_t SEND_TIMES = 3;
    for (uint32_t i = 0; i < SEND_TIMES; i++) {
        Info data[ELEMENT_SIZE] = {
            {true, i, 3.14},
            {true, i, 3.14},
            {true, i, 3.14},
            {true, i, 3.14},
        };
        HDF_LOGI("%{public}s:write smq message %{public}u", __func__, i);
        int status = smq->Write(&data[0], ELEMENT_SIZE, OHOS::MillisecToNanosec(SMQ_WAIT_TIME));
        ASSERT_EQ(status, 0);
    }
}

/*
 * smq type:                             async
 * smq size:                             10
 * write type:                           Non blocking
 * write times:                          5
 * number of elements written each time: 1
 * smq is overflow:                      no
 */
HWTEST_F(SmqTest, SmqTest_005, TestSize.Level1)
{
    // 创建一个元素类型为Info，容量为SMQ_SIZE的共享内存队列对象，读写方式为非阻塞
    std::shared_ptr<SharedMemQueue<Info>> smq = std::make_shared<SharedMemQueue<Info>>(SMQ_SIZE, SmqType::UNSYNC_SMQ);
    constexpr uint32_t ELEMENT_SIZE = 1;

    int32_t ec = g_client->SmqTest(smq, ELEMENT_SIZE);
    ASSERT_EQ(ec, HDF_SUCCESS);

    constexpr uint32_t SEND_TIMES = 5;
    for (uint32_t i = 0; i < SEND_TIMES; i++) {
        Info data[ELEMENT_SIZE] = {
            {true, i, 3.14},
        };
        HDF_LOGI("%{public}s:write smq message %{public}u", __func__, i);
        int status = smq->WriteNonBlocking(&data[0], ELEMENT_SIZE);
        EXPECT_EQ(status, 0);
    }
}

/*
 * smq type:                             async
 * smq size:                             10
 * write type:                           Non blocking
 * write times:                          6
 * number of elements written each time: 2
 * smq is overflow:                      yes
 */
HWTEST_F(SmqTest, SmqTest_006, TestSize.Level1)
{
    std::shared_ptr<SharedMemQueue<Info>> smq = std::make_shared<SharedMemQueue<Info>>(SMQ_SIZE, SmqType::UNSYNC_SMQ);
    constexpr uint32_t ELEMENT_SIZE = 2;

    int32_t ec = g_client->SmqTest(smq, ELEMENT_SIZE);
    ASSERT_EQ(ec, HDF_SUCCESS);

    constexpr uint32_t SEND_TIMES = 6;
    for (uint32_t i = 0; i < SEND_TIMES; i++) {
        Info data[ELEMENT_SIZE] = {
            {true, i, 3.14},
            {true, i, 3.14},
        };
        HDF_LOGI("%{public}s:write smq message %{public}u", __func__, i);
        int status = smq->WriteNonBlocking(&data[0], ELEMENT_SIZE);
        EXPECT_EQ(status, 0);
    }
}

/*
 * smq type:                             async
 * smq size:                             10
 * write type:                           Non blocking
 * write times:                          4
 * number of elements written each time: 4
 * smq is overflow:                      yes
 */
HWTEST_F(SmqTest, SmqTest_007, TestSize.Level1)
{
    std::shared_ptr<SharedMemQueue<Info>> smq = std::make_shared<SharedMemQueue<Info>>(SMQ_SIZE, SmqType::UNSYNC_SMQ);
    constexpr uint32_t ELEMENT_SIZE = 4;

    int32_t ec = g_client->SmqTest(smq, ELEMENT_SIZE);
    ASSERT_EQ(ec, HDF_SUCCESS);

    constexpr uint32_t SEND_TIMES = 4;
    for (uint32_t i = 0; i < SEND_TIMES; i++) {
        Info data[ELEMENT_SIZE] = {
            {true, i, 3.14},
            {true, i, 3.14},
            {true, i, 3.14},
            {true, i, 3.14},
        };
        HDF_LOGI("%{public}s:write smq message %{public}u", __func__, i);
        int status = smq->WriteNonBlocking(&data[0], ELEMENT_SIZE);
        EXPECT_EQ(status, 0);
    }
}
```

接口实现端，仅供参考：

```
static constexpr uint32_t SMQ_ELEMENT_SIZE_MAX = 5;
static constexpr uint32_t SMQ_WAIT_TIME = 100;

int32_t SmqTestService::SmqTest(const std::shared_ptr<SharedMemQueue<Info>>& smq, uint32_t elementSize)
{
    HDF_LOGI("%{public}s:element size:%{public}u", __func__, elementSize);

    std::thread t([smq, elementSize]() {
        HDF_LOGI("SmqTest: smq read thread start, element size:%{public}u", elementSize);

        while (true) {
            Info data[SMQ_ELEMENT_SIZE_MAX] = {};
            int ret;
            if (smq->GetMeta()->GetType() == SmqType::SYNCED_SMQ) {
                ret = smq->Read(&data[0], elementSize, OHOS::MillisecToNanosec(SMQ_WAIT_TIME));
            } else {
                ret = smq->ReadNonBlocking(&data[0], elementSize);
            }

            if (ret != HDF_SUCCESS) {
                HDF_LOGE("failed to read message from smq, %{public}d", ret);
                break;
            }

            for (uint32_t i = 0; i < elementSize; i++) {
                HDF_LOGI("read message form smq, info[%{public}u]:{%{public}s, %{public}d, %{public}lf}",
                    i, data[i].m1 ? "true" : "false", data[i].m2, data[i].m3);
            }
        }
    });
    t.detach();

    return HDF_SUCCESS;
}
```



#### 共享内存

Ashmem为IDL支持的共享内存类型，当前仅支持C++，IDL共享内存数据类型与Java数据类型，C/C++数据类型的对应关系如下表所示：

| HarmonyOS IDL数据类型 | C++数据类型  | C数据类型 | Java数据类型 |
| --------------------- | ------------ | --------- | ------------ |
| Ashmem                | sptr<Ashmem> | 不支持    | 不支持       |

共享内存头文件路径：

```
//utils/native/base/include/ashmem.h
```



#### 文件描述符

FileDescriptor为IDL支持的文件描述符类型，当前仅支持C/C++，FileDescriptor数据类型与Java数据类型，C/C++数据类型的对应关系如下表所示：

| HarmonyOS IDL数据类型 | C++数据类型 | C数据类型 | Java数据类型 |
| --------------------- | ----------- | --------- | ------------ |
| FileDescriptor        | int         | int       | 不支持       |



#### NativeBuffer

NativeBuffer数据类型与Java数据类型，C/C++数据类型的对应关系如下表所示：

| HarmonyOS IDL数据类型 | C++数据类型  | C数据类型    | Java数据类型 |
| --------------------- | ------------ | ------------ | ------------ |
| NativeBuffer          | NativeBuffer | BufferHandle | 不支持       |



#### Pointer

Pointer数据类型表示**地址**，仅适用于**直通模式**，C/C++数据类型的对应关系如下表所示：

| HarmonyOS IDL数据类型 | C++数据类型 | C数据类型 | Java数据类型 |
| --------------------- | ----------- | --------- | ------------ |
| Pointer               | void *      | void *    | 不支持       |

