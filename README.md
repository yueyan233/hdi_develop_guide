# HDI开发指导

#### 介绍

HDI开发指导文档



#### 支持情况

| 系统量级 | 语言 | 模式        | 支持情况 |
| -------- | ---- | ----------- | -------- |
| L2       | C++  | IPC         | YES      |
| L2       | C++  | Passthrough | YES      |
| L2       | C    | IPC         | YES      |
| L2       | C    | Passthrough | YES      |
| L1       | C    | -           | NO       |

